#  Copyright (c) 1995-8,2001  Martin Schulze <joey@infodrom.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA.

#  Makefile for uucpsend

CC = gcc
CFLAGS = -O3 -Wall
DEST=
MAN=man
NEWS=news
OBJS=uucpsend.o log.o
STRIP=-s

uucpsend: $(OBJS)

clean:
	rm -f $(OBJS) uucpsend

install:
	for d in $(DEST)/usr/lib/news/bin $(DEST)/etc/news $(DEST)/usr/man/man{5,8}; \
	do \
	  if [ ! -d $$d ]; then install -m 755 -o root -g root -d $$d ; fi; \
	done
	install -m 755 -o root -g root $(STRIP) uucpsend $(DEST)/usr/lib/news/bin
	install -m 644 -o $(NEWS) -g $(NEWS) uucpsend.ctl $(DEST)/etc/news
	install -m 644 -o $(MAN) -g $(MAN) uucpsend.8 $(DEST)/usr/man/man8
	install -m 644 -o $(MAN) -g $(MAN) uucpsend.ctl.5 $(DEST)/usr/man/man5


log.o: log.c log.h
uucpsend.o: uucpsend.c paths.h
