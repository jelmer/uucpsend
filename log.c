/*
   Copyright (c) 1995-8,2001  Martin Schulze <joey@infodrom.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA.
*/

/*
**  Write log into a file and not via syslog
*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>

FILE *logfile = NULL;

char *months[] =
{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

void
openlogfile(char *name)
{
  logfile = fopen(name,"a");
}

void
closelogfile()
{
  if (logfile)
    (void)fclose(logfile);
}

void
filelog(char *format, ...)
{

  va_list argp;
  time_t timer;
  struct tm *loctime;

/* Output should be: 
 *    May 21 10:54:18
 */

  if (logfile) {
    timer = time ( NULL );
    loctime = localtime(&timer);
    fprintf(logfile, "%s %2d %2d:%02d:%02d ",
	    months[loctime->tm_mon], loctime->tm_mday, loctime->tm_hour, loctime->tm_min, loctime->tm_sec); 
    va_start(argp, format);
    vfprintf(logfile, format, argp);
    fprintf(logfile, "\n");
    va_end(argp);
    fflush(logfile);
  }
  else {
    va_start(argp, format);
    vfprintf(stderr, format, argp);
    fprintf (stderr, "\n");
    va_end(argp);
    fflush(stderr);
  }
}
